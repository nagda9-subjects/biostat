---
title: "R Notebook"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.

***

This tutorial is based on the materials: **https://people.ok.ubc.ca/jpither/modules/biol202_home.html** 
Useful links:
markdown cheatsheet: **https://rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf**
data import, transformation, visualisation cheasheets: **https://www.aridhia.com/blog/r-for-researchers-8-essential-cheatsheets-for-research-data-analysis/**

***

# Practice 2: t-test, confidence interval


## Getting started

In this tutorial we will learn about the following:

* the Student's *t* distribution
* Inference for a normal population: Student's *t*-test 
* the 95% confidence interval for a mean

* * *

### Required packages

* `tigerstats`
* `tidyr`
* `car`

```{r}
library(tigerstats)
library(tidyr)
library(car)

```

* * *

### Required data

```{r}
bodytemp <- read.csv(url("https://people.ok.ubc.ca/jpither/datasets/bodytemp.csv"), header = TRUE)
inspect(bodytemp)
blackbird <- read.csv(url("https://people.ok.ubc.ca/jpither/datasets/blackbird.csv"), header = TRUE)
inspect(blackbird)
students <- read.csv(url("https://people.ok.ubc.ca/jpither/datasets/students.csv"), header = TRUE)
inspect(students)
```

* * *

## The *t* distribution for sample means

As described in the text on pages 304-306, the *t* distribution resembles the standard normal distribution (the *Z* distribution), but is slighly fatter in the tails.  

The *t* distribution is what we use in practice, i.e. when we're working with samples of data, when drawing inferences about normal populations for which $\mu$ and $\sigma$ are unknown.  

* * *

### Calculating probabilities from a *t* distribution

As with the *Z* distribution, we can look up probability values (areas under the curve) associated with values of *t* in tables a table
Unlike the *Z* distribution, the *t* distribution changes shape depending upon the **degrees of freedom**:  

*df* = *n* - 1  

**Example**:  

What is the probability (*P*-value) associated with obtaining a *t* statistic value of 2.1 or larger, given a sample size *n* = 11 (*df* = 10)?  

To calculate this, we use the `pt` function in the base package:  

```{r}
pt(2.1, df = 10, lower.tail = FALSE) # note lower.tail argument
```

* * *

### Finding critical values of *t*

Without a computer, one would use tables to look up **critical values** of a test statistic like the *t* statistic.  

In R, we can use the `qt` function to find the critical value of *t* associated with a given $\alpha$ level and **degrees of freedom**.  

For instance, if we were testing a 2-tailed hypothesis, with $\alpha$ = 0.05, and sample size *n* = 11, here's the code:

```{r}
alpha <- 0.05 # define alpha
n <- 11  # define n
upper.crit <- qt(alpha/2, df = n - 1, lower.tail = FALSE) # if 2-tailed, divide alpha by 2
lower.crit <- qt(alpha/2, df = n - 1, lower.tail = TRUE) # if 2-tailed, divide alpha by 2
c(lower.crit, upper.crit)
```

This shows the lower and upper critical values of *t* associated with *df* = 10 and $\alpha$ = 0.05.  

If we had a different value of $\alpha$, say $\alpha$ = 0.1, and the same sample size here's the code:

```{r}
alpha <- 0.10 # define alpha
upper.crit <- qt(alpha/2, df = n - 1, lower.tail = FALSE) # if 2-tailed, divide alpha by 2
lower.crit <- qt(alpha/2, df = n - 1, lower.tail = TRUE) # if 2-tailed, divide alpha by 2
c(lower.crit, upper.crit)
```

We generally don't use the `qt` and `pt` functions much on their own, but now you know what they can be used for!  

* * *

## One-sample _t_-test      

Here we are going to learn our first statistical test for testing hypotheses about a numeric response variable, specifically one whose frequency distribution in the population is normally distributed.  

* * *

### Hypothesis statement

We'll use body temperature data for this example  

Americans are taught as kids that the normal human body temperature is 98.6 degrees Farenheit.  

Are the data consistent with this assertion?  

The hypotheses for this test:  

**H~0~**: The mean human body temperature is 98.6$^\circ$F.  
**H~A~**: The mean human body temperature is not 98.6$^\circ$F.  

* We'll use an $\alpha$ level of 0.05.  
* It is a two-tailed alternative hypothesis  
* We'll visualize the data, and interpret the graph
* We'll use a one-sample *t*-test test to test the null hypothesis, because we're dealing with continuous numerical data, and using a sample of data to draw inferences about a population mean $\mu$   

The assumptions of the one-sample *t*-test are as follows:  

* the sampling units are randomly sampled from the population
* the variable is normally distributed in the population  

* We'll calculate our test statistic
* We'll calculate the *P*-value associated with our test statistic
* We'll provide a good concluding statement that includes a 95% confidence interval for the mean

* * *

### Visualize the data

Let's view a histogram of the body temperatures.  

We can use either of two different functions: the `histogram` function or the `hist` function.  

```{r}
histogram(~ temperature, data = bodytemp,
          type = "count",
          breaks = seq(from = 97, to = 100.5, by = 0.5),
          col = "firebrick",
          las = 1,
          xlab = "Body temperature (degrees F)",
          ylab = "Frequency", 
          main = "")
```

Here's the code for using the `hist` function:

```{r}
hist(bodytemp$temperature,
          col = "firebrick",
          las = 1,
          xlab = "Body temperature (degrees F)",
          ylab = "Frequency", 
          main = "")
```

The advantage of the `histogram` function is it uses the same syntax that we used to (using the `~` symbol), but the disadvantage is that to get the bars of the histogram to line up with the x-axis tick marks, we need to specify the "breaks" using the `breaks` argument (as done in the R chunk above for the `histogram` function). If you used the `histogram` function, it is recommended that you specify the "breaks" using the `seq` command, as above.   

The advantage of the `hist` function is that it is lines up the bars with tick marks by default. The disadvantage is that it does not use the same syntax that we're used to.  

> It is recommended that you use the `hist` command when visualizing the frequency distribution of a single numeric variable.  

We can see in Figure 1 that the modal temperature among the 25 subjects is between 98.5 and 99$^\circ$F, which is consistent with conventional wisdom, but there are 7 people with temperature below 98$^\circ$F, and 5 with temperatures above 99$^\circ$F. The frequency distribution is unimodal but not particularly symetrical.   

* * *

### Conduct the *t*-test

We use the `t.test` command from the `mosaic` package (installed as part of the `tigerstats` package) to conduct a one-sample _t_-test.  

**NOTE**: The base `stats` package that automatically loads when you start R also includes a `t.test` function, but it doesn't have the same functionality as the `mosaic` package version.  **BE SURE** to load the `tigerstats` package prior to using the `t.test` function! 

**TIP**: We can ensure that the correct function is used by including the package name before the function name, separated by colons:

```
mosaic::t.test
```

This function is used for both one-sample and two-sample _t_-tests and for calculating 95% confidence intervals for a mean.  

Because this function has multiple purposes, be sure to pay attention to the arguments.  

You can find out more about the function at the `tigerstats` website [here](http://homerhanumat.com/tigerstats/ttestGC.html).

Let's think again about what the test is doing.  In essence, it is taking the observed sample mean, transforming it to a value of *t* to fit on the _t_ distribution.  This is analogous to what we learned to do with "Z-scores", but here we're using the _t_ distribution instead of the Z distribution, because we don't know the true population parameters ($\mu$ and $\sigma$), and we're dealing with sample data.  

Once we have our observed value of the test statistic _t_, we can calculate the probability of observing that value, or one of greater magnitude, _assuming the null hypothesis were true_.

Here's the code, and we include the null hypothesized body temperature:  


```{r}
body.ttest <- mosaic::t.test(~ temperature, data = bodytemp,
        mu = 98.6,
        alternative = "two.sided",
        conf.level = 0.95) 
body.ttest
```

The output includes a 95% confidence interval for $\mu$, the calculated test statistic *t*, the degrees of freedom, and the associated *P*-value.  

The observed *P*-value for our test is larger than our $\alpha$ level of 0.05.  We therefore fail to reject the null hypothesis.

* * *

### Concluding statement

We have no reason to reject the hypothesis that the mean body temperature of a healthy human is 98.6$^\circ$F (one-sample _t_-test; _t_ = `r round(body.ttest$statistic,2)`; _n_ = 25 or *df* = 24; _P_ = `r round(body.ttest$p.value,3)`).  

* * *

## Confidence intervals for an estimate of $\mu$

Now we will learn how to calculate confidence intervals correctly.  

Here's the code, again using the `t.test` function, but this time, if all we wish to do is calculate confidence intervals, we **do not** include a value for "mu" (as we do when we are conducting a one-sample *t*-test):  

```{r}
body.conf <- mosaic::t.test(~ temperature, data = bodytemp,
        alternative = "two.sided",
        conf.level = 0.95)
body.conf
```

It is good practice to report confidence intervals (or any measure of precision) to 3 decimal places, and to include units.  

We can do this in R as follows:  

```{r}
lower.cl <- round(body.conf$conf.int[1],3)
upper.cl <- round(body.conf$conf.int[2],3)
c(lower.cl, upper.cl)
```

**TIP**: You can get R to provide "inline" evaluation of code.  For instance, type the following code in the main text area, NOT in a chunk:

```
The 95% confidence interval for the mean is `r round(body.conf$conf.int[1],3)` to `r round(body.conf$conf.int[2],3)` $^\circ$F. 
```

... and you will get the following:  

The 95% confidence interval for the mean is `r round(body.conf$conf.int[1],3)` to `r round(body.conf$conf.int[2],3)` $^\circ$F. 

Now we can re-write our concluding statement and include the confidence interval:

We have no reason to reject the hypothesis that the mean body temperature of a healthy human is 98.6$^\circ$F (one-sample _t_-test; _t_ = `r round(body.ttest$statistic,2)`; _n_ = 25 or *df* = 24; _P_ = `r round(body.ttest$p.value,3)`; 95% CI: `r round(body.ttest$conf.int[1],3)` - `r round(body.ttest$conf.int[2],3)` $^\circ$F).  

* * *

## Paired *t*-test

We'll use the `blackbird` dataset for this example.  

For 13 red-winged blackbirds, measurements of antibodies were taken before and after implantation with testosterone.  Thus, the same bird was measured twice. Clearly, these measurements are not independent, hence the need for a "paired" _t_-test.  

* * *

### Data structure: Long versus Wide format  

Let's look at how the data are stored, as this key to deciding how to proceed:  

```{r}
blackbird
```

The data frame has 26 rows, and includes 3 variables, the first of which "blackbird" simply keeps track of the individual ID of blackbirds.  

The response variable of interest, "Antibody" represents antibody production rate measured in units of natural logarithm (ln) 10^{-3} optical density per minute (ln[mOD/min]).  

The factor variable `time` that has two levels: "After" and "Before".  

These data are stored in **long format**, which is the ideal format for storing data. I encourage you to read this [webpage](https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html) regarding "tidy data".  

Sometimes you may get data in **wide format**, in which case, for instance, we would have a column for the "Before" antibody measurements and another column for the "After" measurements.  

**It is always preferable** to work with long-format data.  

Consult the following [webpage](http://www.cookbook-r.com/Manipulating_data/Converting_data_between_wide_and_long_format/) for instructions on using the `tidyr` package for converting between wide and long data formats.  

With our data in the preferred long format, we can proceed with our hypothesis test.  

* * *

### Hypothesis statements

The hypotheses for this paired _t_-test focus on the mean of the *differences* between the paired measurements, denoted by $\mu$~d~:  

H~0~: The mean change in antibody production after testosterone implants was zero ($\mu$~d~ = 0).  
H~A~: The mean change in antibody production after testosterone implants was not zero ($\mu$~d~ $\neq$ 0).  

Steps to a hypothesis test:  

* We'll use an $\alpha$ level of 0.05.  
* It is a two-tailed alternative hypothesis  
* We'll visualize the data, and interpret the output
* We'll use a paired *t*-test test to test the null hypothesis, because we're dealing with "before and after" measurements taken on the same individuals, and drawing inferences about a population mean $\mu$~d~ using sample data  
* We'll check the assumptions of the test (see below)
* We'll calculate our test statistic
* We'll calculate the *P*-value associated with our test statistic
* We'll calculate a 95% confidence interval for the mean difference
* We'll provide a good concluding statement that includes a 95% confidence interval for the mean difference

* * *

### Assumptions of the paired *t*-test  

The assumptions of the paired *t*-test are the same as the assumptions for the one-sample *t*-test:  

* the sampling units are randomly sampled from the population
* the differences have a normal distribution in the population (each group of measurements need not be normally distributed) 
 
* * *

### Visualize the data      

The best way to visualize the data for a paired *t*-test is to create a **histogram** of the calculated *differences* between the paired observations.  

We can calculate the differences between the "After" and "Before" measurements in a couple different ways.  

First, we can use the `filter` command from the `tidyr` package (you previously used the same `filter` command from the `dplyr` package, which is part of the `tidyr` package), and use the `$` symbol to extract only the variable of interest:  

```{r}
antibody.diffs <- filter(blackbird, time == "After")$Antibody - filter(blackbird, time == "Before")$Antibody
```

Or we can use simple subsetting:
```{r}
antibody.diffs <- blackbird[blackbird$time == "After", "Antibody"] - blackbird[blackbird$time == "Before", "Antibody"]
```

Either way, our result is a vector of 13 differences, which we can now visualize with a histogram.  

**NOTE**: Although previously we've used the `histogram` function to generate histograms, it is often easier to get easily-interpreted histograms using the base package `hist` function. We'll use this function now.

We can also use the `segments` command to add a vertical dashed line that corresponds with the hypothesized mean difference of zero:    

```{r}
hist(antibody.diffs, nclass = 8, ## asks for 8 bars
     xlab = "Antibody production rate (ln[mOD/min])",
     las = 1, main = "",
     col = "lightgrey")
segments(x0 = 0, y0 = 0, x1 = 0, y1 = 5, lty = 2, lwd = 2, col = "red") # add vertical dashed line at hypothesized mu
```

With such a small sample size (13), the histogram is not particularly informative. But we do see most observations are just above zero.  

* * *

### Check assumptions

The paired *t*-test assumes:  

* the sampling units are randomly sampled from the population
* the paired differences have a normal distribution in the population  

We assume the first assumption is met.  


Let's first check the normality assumption visually using a **Normal Quantile Plot**, and note that we're assessing the single response variable representing the **difference** in before and after measurements:  

```{r}
qqnorm(antibody.diffs, las = 1, main = ""); 
qqline(antibody.diffs)
```

If the observations come from a normal distribution, they will generally fall close to the straight line.  

Here, we would conclude:  

> "The normal quantile plot shows that the data generally fall close to the line (except perhaps the highest value), suggesting that the data are drawn from a normal distribution."    

And now a formal goodness of fit test, called the **Shapiro-Wilk Normality Test**, which tests the null hypothesis that the data are sampled from a normal distribution:    

```{r}
shapiro.result <- shapiro.test(antibody.diffs)
shapiro.result
```

Given that the *P*-value is large (and much greater than 0.05), there is no reason to reject the null hypothesis. Thus, our normality assumption is met.  

When testing the normality assumption using the Shapiro-Wilk test, there is no need to conduct all the steps associated with a hypothesis test. Simply report the results of the test (the test statistic `W` value and the associated *P*-value).  

For instance: "A Shapiro-Wilk test revealed no evidence against the assumption that the data are drawn from a normal distribution (*W* = `r round(shapiro.result$statistic,2)`, *P*-value = `r round(shapiro.result$p.value,3)`)."  

* * *

### Conduct the test

We can conduct a **paired t-test** in two different ways:  

* conduct a one-sample *t*-test on the _differences_ using the `t.test` function and methods you learned at the first part of the tutorial.

* conduct a paired *t*-test using the `t.test` function and the argument `paired = TRUE`.


* * *

1. Paired *t*-test with `t.test`  

* using the `antibody.diffs` vector you created above (representing the differences in antibody production rates), conduct all the steps of a hypothesis test, with the null hypothesis being that $\mu$~d~ = 0.

* * *

Let's proceed with the test, remembering to include the `mosaic` package name prior to the function name, to ensure we use the correct function:

```{r}
blackbird.ttest <- mosaic::t.test(Antibody ~ time, data = blackbird, paired = TRUE, conf.level = 0.95)
blackbird.ttest
```

**NOTE**: When you specify `paired = TRUE`, the `t.test` function assumes that the observations are ordered identically in each group (the "Before" and "After" groups).  

The output from the `t.test` function includes the calculated value of the test statistic *t*, the degrees of freedom (df), the *P*-value associated with the calculated test statistic, the 95% confidence interval for the difference, and the sample-based estimate of the difference (mean of the difference).  

We see that the *P*-value of `r round(blackbird.ttest$p.value, 3)` is larger than our stated $\alpha$ (0.05), hence we do not reject the null hypothesis.  

Let's use R to figure out what the *critical value* of *t* is for *df* = `r blackbird.ttest$parameter`:  

```{r}
alpha <- 0.05 # define alpha
n <- 13 
upper.crit <- qt(alpha/2, df = n - 1, lower.tail = FALSE) # if 2-tailed, divide alpha by 2
lower.crit <- qt(alpha/2, df = n - 1, lower.tail = TRUE) # if 2-tailed, divide alpha by 2
c(lower.crit, upper.crit)
```

This shows the lower and upper critical values of *t* associated with *df* = `r blackbird.ttest$parameter` and $\alpha$ = 0.05.  

* * *

### Concluding statement

Given that the `t.test` function calculated the 95% confidence interval for the difference for us, we need not do any additional steps.  

We fail to reject the null hypothesis, and conclude that there was no change in antibody production after testosterone implants (paired *t*-test; *t* = `r round(blackbird.ttest$statistic,2)`; *P*-value = `r round(blackbird.ttest$p.value,3)`; 95% confidence limits: `r round(blackbird.ttest$conf.int[1],3)`, `r round(blackbird.ttest$conf.int[2],3)`).  

* * *

## Two sample *t*-test

Have a look at the `students` dataset:  

```{r}
inspect(students)
```

These data include measurements taken on 154 students.  

Note that the categories in the `dominant_eye` variable are "r" and "l"


### Hypothesis statement

H~0~: The mean height of "right eyed" and "left eyed" students is the same ($\mu$~M~ = $\mu$~F~).  
H~A~: The mean height of "right eyed" and "left eyed" students is not the same ($\mu$~M~ $\neq$ $\mu$~F~).  

Steps to a hypothesis test:  

* We'll use an $\alpha$ level of 0.05.  
* It is a two-tailed alternative hypothesis  
* We'll visualize the data, and interpret the output
* We'll use a 2-sample *t*-test test to test the null hypothesis, because we're dealing with numerical measurements taken on independent within two independent groups, and drawing inferences about population means $\mu$ using sample data  
* We'll check the assumptions of the test (see below)
* We'll calculate our test statistic
* We'll calculate the *P*-value associated with our test statistic
* We'll calculate a 95% confidence interval for the difference ($\mu$~M~ - $\mu$~F~)
* We'll provide a good concluding statement that includes a 95% confidence interval for the mean difference ($\mu$~M~ - $\mu$~F~)  

* * *

### Assumptions of the 2-sample *t*-test  

The assumptions of the 2-sample *t*-test are the same as the assumptions for the one-sample *t*-test:  

* each of the two samples is a random sample from its population
* the numerical variable is normally distributed in each population
* the variance (and thus standard deviation) of the numerical variable is the same in both populations  

**NOTE**: Read bottom of page 340 in the text, which describes how *robust* this test is to violations of the assumptions.  
 
* * *

### Visualize the data      

Here we want to visualize height in relation to eye.  

We use a stripchart for relatively small sample sizes in each group (e.g. < 20), and a boxplot otherwise.  

Let's calculate sample sizes by tabulating the frequency of each eye dominance:  

```{r}
samp.sizes <- xtabs(~ dominant_eye, data = students) 
samp.sizes
```

So, large samples sizes in each group, therefore a boxplot is warranted.  

```{r}
boxplot(height_cm ~ dominant_eye, data = students,
        ylab = "Height (cm)",
        xlab = "Dominant_eye",
        las = 1)  # orients y-axis tick labels properly
```


**TIP**: You can create even better boxplots using the `ggplot2` package, as described [here](http://ggplot2.tidyverse.org/reference/geom_boxplot.html).  

* * *

### Check assumptions

The assumptions of the 2-sample *t*-test are the same as the assumptions for the one-sample *t*-test:  

* each of the two samples is a random sample from its population
* the numerical variable is normally distributed in each population
* the variance (and thus standard deviation) of the numerical variable is the same in both populations  

* * *

**Test for normality**  

Now let's check the normality assumption by plotting a normal quantile plot for each group. We use the `par` function to enable 2 graphs positioned side by side:  

```{r}
par(mfrow = c(1,2)) # create one row of 2 columns for graphs
qqnorm(students$height_cm[students$dominant_eye == "l"], las = 1, main = "Left");
qqline(students$height_cm[students$dominant_eye == "l"]) # add the line
qqnorm(students$height_cm[students$dominant_eye == "r"], las = 1, main = "Right");
qqline(students$height_cm[students$dominant_eye == "r"]) # add the line
```

We see that the male data are a little bit off the line, but we know that, thanks to the **central limit theorem**, the 2-sample *t*-test is robust to violations of non-normality when one has large sample sizes. Thus, we'll proceed with testing the next assumption (there's no need to conduct a Shapiro-Wilk's test).  

* * *

### Conduct the test

We use the `t.test` function again, but this time we make sure to set the `paired` argument to `FALSE`.  

**NOTE**: `var.equal = TRUE`: regular 2-sample *t*-test. 
`var.equal = FALSE`: **Welch's _t_-test**, which is appropriate when variances are unequal (see the help file for the function).

```{r}
height.ttest <- t.test(height_cm ~ dominant_eye, data = students, paired = FALSE, var.equal = TRUE, conf.level = 0.95)
height.ttest

height.ttest <- t.test(height_cm ~ dominant_eye, data = students, paired = FALSE, var.equal = FALSE, conf.level = 0.95)
height.ttest
```

We see that the test produced a quite high *P*-value, so we accept the null hypothesis.  


Note also that the output includes a **confidence interval** for the _difference_ in group means. We need to include this in our concluding statement. 

So, eye dominance does not have a statistically significant effect on hight.

* * *

